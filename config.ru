require_relative './app/controllers/contact_controller'
require 'otr-activerecord'
require 'sidekiq'

use OTR::ActiveRecord::ConnectionManagement
use Rack::Reloader, 0
run Rack::Cascade.new([Rack::File.new("public"), ContactController])
