require 'erb'
require 'active_record'

class BaseController

  def render(temp)
    path = File.expand_path("../../views/#{temp}", __FILE__)
    ERB.new(File.read(path)).result(binding)
  end

end
