require_relative 'base_controller'
require_relative '../../lib/background_service'


class ContactController < BaseController

  def self.call(env)
   new(env).mail.finish
  end

  def initialize(env)
    @request = Rack::Request.new(env)
  end


  def mail
    case @request.path

    when "/" then Rack::Response.new(render('contact.html.erb'))
      when "/mail" then Rack::Response.new(render('done.html.erb')) do
        puts "----> Job Executing..."
        email = EmailWorker.perform_async(@request.params)
        #email.save
        render 'done.html.erb'
      end

      else Rack::Response.new('Not found', 404)
    end
  end

  def create
    @contact = Contact.new(@request.params)
    respond_to do |format|
      if @request.save
        ContactMailer.with(user: @request).welcome_email.deliver_later

        format.html { redirect_to(@request.params, notice: 'User was successfully created.') }
        format.json { render json: @request.params, status: :created, location: @user }
      else
        format.html { render action: 'new' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end
end
