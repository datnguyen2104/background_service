require 'action_mailer'


class ContactMailer < ActionMailer::Base
  default from: 'notifications@contact.com'

  def welcome_email(user)
    @user = user
    mail(to: @user, subject: "Welcome to My Awesome Site #{@user}") do |format|
      format.html
    end
  end
end
