

#Bundler.require(:default, ENV["development"].to_sym)
require 'bundler/setup'
Bundler.require(:default, :development)
require 'action_mailer'
require './lib/background_service'



ActiveRecord::Base.establish_connection(
  adapter:  'postgresql',
  host:     'localhost',
  username: 'thanhdat1',
  password: '123456',
  database: 'active_record'
)

require "letter_opener"
ActionMailer::Base.add_delivery_method :letter_opener, LetterOpener::DeliveryMethod, :location => File.expand_path('../tmp/letter_opener', __FILE__)
ActionMailer::Base.delivery_method = :letter_opener
ActionMailer::Base.view_paths = File.expand_path('../../../background_service/app/views/', __FILE__)
